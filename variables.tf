variable "zone_id" {
  description = "ID of Domain zone name"
}

variable "record" {
  default     = []
  description = "A List of simple records to add to the zone"
  type        = "list"
}

variable "record_alias" {
  default     = []
  description = "A List of the alias records to add to the zone"
  type        = "list"
}
