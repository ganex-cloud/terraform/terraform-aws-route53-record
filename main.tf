# Simple records
resource "aws_route53_record" "default" {
  count   = "${length(var.record)}"
  zone_id = "${var.zone_id}"
  name    = "${lookup(var.record[count.index], "name")}"
  type    = "${lookup(var.record[count.index], "type")}"
  ttl     = "${lookup(var.record[count.index], "ttl", "300")}"

  records = [
    "${split(",", lookup(var.record[count.index], "record"))}",
  ]
}

# Alias records
resource "aws_route53_record" "alias" {
  count   = "${length(var.record_alias)}"
  zone_id = "${var.zone_id}"
  name    = "${lookup(var.record_alias[count.index], "name")}"
  type    = "${lookup(var.record_alias[count.index], "type")}"

  alias {
    name                   = "${lookup(var.record_alias[count.index], "record")}"
    zone_id                = "${lookup(var.record_alias[count.index], "zone_id", var.zone_id)}"
    evaluate_target_health = "${lookup(var.record_alias[count.index], "evaluate_target_health", "false")}"
  }
}
