module "route53-domain-com-br-records" {
  source  = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-route53-record.git?ref=master"
  zone_id = "${data.terraform_remote_state.project.domain-com-br_zone_id}"

  record_alias = [
    {
      name   = "test4"
      type   = "A"
      record = "teste1.domain.com.br"
    },
  ]
}
