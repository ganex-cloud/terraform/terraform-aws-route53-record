module "route53-domain-com-br-records" {
  source  = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-route53-record.git?ref=master"
  zone_id = "${data.terraform_remote_state.project.domain-com-br_zone_id}"

  record = [
    {
      name   = "test1"
      type   = "A"
      record = "8.8.8.8"
    },
    {
      name   = "test2"
      type   = "CNAME"
      record = "google.com"
    },
    {
      name   = "test3"
      type   = "TXT"
      record = "v=spf1 include:_spf.google.com include:servers.mcsv.net include:mail.zendesk.com include:sendgrid.net ~all"
    },
    {
      name   = "k8s"
      type   = "NS"
      ttl    = "172800"
      record = "${module.route53-XXXXX.name_servers}"
    },
  ]
}
